require 'retrospective'
require 'team'

RSpec.describe Retrospective do
  let(:teams) do
    {
      'Plan' => {
        'label' => 'plan',
        'project' => 'plan',
        'owner' => 'smcgivern',
        'mention_team' => false,
        'create_discussions' => false,
        'create_individual_discussions' => false,
        'create_parent_topic' => false
      },
      'Create' => {
        'label' => 'group::code%20review',
        'project' => 'create-stage%2fcode-review',
        'owner' => 'm_gill',
        'mention_team' => false,
        'create_discussions' => false,
        'create_individual_discussions' => false,
        'create_parent_topic' => true
      }
    }
  end
  let(:subject) { Retrospective.new('1234') }

  before do
    allow(Team).to receive(:hash_from_file).and_return(teams)
  end

  describe 'create parent topic discussion' do
    let(:parent_topic_true) { Team.find('Create') }
    let(:parent_topic_false) { Team.find('Plan') }
    let(:issue_url) { 'nothing.com' }
    let(:success_response) { double(code: 200) }

    context 'when true' do
      it 'should include a new thread' do
        expect_any_instance_of(GitlabApi).to receive(:post).with(a_string_including(issue_url), body: { body: a_string_including('company retrospective') }).and_return(success_response)

        subject.create_discussions(issue_url, team: parent_topic_true, dry_run: false)
      end
    end

    context 'when false' do
      it 'should not include a new thread' do
        expect_any_instance_of(GitlabApi).not_to receive(:post).with(a_string_including(issue_url), body: { body: a_string_including('company retrospective') })

        subject.create_discussions(issue_url, team: parent_topic_false, dry_run: false)
      end
    end
  end
end
