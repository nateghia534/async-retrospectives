require 'date'
require 'erb'

require './lib/gitlab_api'

class DescriptionTemplate
  attr_reader :team, :release, :token

  def initialize(team, release, token:)
    @team = team
    @release = release
    @token = token
  end

  def issue_query_prefix
    # https://docs.gitlab.com/ee/api/issues.html
    if @team.query_all_groups?
      'issues?scope=all&'
    else
      'groups/gitlab-org/issues?' # The default queries gitlab-org only
    end
  end

  def merge_request_query_prefix
    # https://docs.gitlab.com/ee/api/merge_requests.html
    if @team.query_all_groups?
      'merge_requests?scope=all&'
    else
      'groups/gitlab-org/merge_requests?'  # The default queries gitlab-org only
    end
  end

  def get_with_auth?
    # default is false - this sets to true when querying all groups (e.g. may include non-public groups)
    # consider making the retrospective confidential if using query_all_groups
    # note there are some queries where auth is always set to true (counts, sums)
    @team.query_all_groups? ? true : false
  end

  def template_issue_url_prefix
    if @team.query_all_groups?
      'dashboard/issues?scope=all&' # Accounts for other projects (e.g. gitlab-services), scope=all ensures issues created by others are included
    else
      'groups/gitlab-org/-/issues?' # The default queries gitlab-org only
    end
  end

  def result_with_hash(hash)
    binding_hash = {
      team: team,
      release: release,
      due_date: Date.today.next_month.strftime('%Y-%m-26')
    }

    if hash[:updating_description]
      binding_hash.merge!(
        due_date: Date.today.strftime('%Y-%m-26'),
        deliverables: deliverables,
        issue_count: issue_count,
        total_weight: total_weight,
        merge_request_count: merge_request_count,
        slipped: slipped,
        follow_up: follow_up,
        current_retrospective: current_retrospective,
        issue_url_prefix: hash[:query_all_groups] === true ? 'dashboard/issues?scope=all&' : 'groups/gitlab-org/-/issues?'
      )
      if team.additional_label
        hash[:additional_label] = team.additional_label
        hash[:issues_with_additional_label] = issues_with_additional_label
      end
    end

    binding_hash.merge!(hash)

    include_template = lambda do |filename|
      load_template(filename).result_with_hash(binding_hash)
    end

    binding_hash[:include_template] = include_template

    template.result_with_hash(binding_hash)
  end

  def current_issue_url
    current_retrospective&.fetch('web_url') || 'http://does.not/exist'
  end

  def current_api_path
    "projects/gl-retrospectives%2F#{team.project}/issues/#{current_retrospective&.fetch('iid') || -1}"
  end

  def current_retrospectives_api_path(extra_labels = [])
    labels = ['retrospective'].concat(extra_labels)
    "projects/gl-retrospectives%2F#{team.project}/issues?labels=#{labels.join(',')}&state=opened&search=#{release}"
  end

  def deliverables
    @deliverables ||=
      api.get("#{issue_query_prefix}labels=#{team.label},Deliverable&state=closed&milestone=#{release}", auth: get_with_auth?)
  end

  def slipped
    @slipped ||=
      api.get("#{issue_query_prefix}labels=#{team.label},missed%3A#{release}", auth: get_with_auth?)
  end

  def follow_up
    @follow_up ||=
      api.get("projects/gl-retrospectives%2F#{team.project}/issues?labels=follow-up&state=opened", auth: get_with_auth?)
  end

  def issues_with_additional_label
    @issues_with_additional_label ||=
        api.get("#{issue_query_prefix}labels=#{team.label},#{team.additional_label}&state=closed&milestone=#{release}", auth: get_with_auth?)
  end

  def current_retrospective
    @current_retrospective ||= current_retrospectives.first
  end

  def current_retrospectives
    if current_retrospectives_with_team_label.size > 0
      current_retrospectives_with_team_label
    else
      current_retrospectives_without_team_label
    end
  end

  def current_retrospectives_with_team_label
    @current_retrospectives_with_team_label ||= api.get(current_retrospectives_api_path([team.label]), auth: true)
  end

  def current_retrospectives_without_team_label
    @current_retrospectives_without_team_label ||= api.get(current_retrospectives_api_path, auth: true)
  end

  def issue_count
    @issue_count ||=
      api.count("#{issue_query_prefix}labels=#{team.label}&state=closed&milestone=#{release}")
  end

  def total_weight
    @total_weight ||= begin
      issues = api.get("#{issue_query_prefix}labels=#{team.label}&state=closed&milestone=#{release}", auth: true)
      sum = issues.sum { |i| i['weight'] || 0 }

      return sum if issues.count == issues.headers['X-Total'].to_i

      "#{sum}+"
    end
  end

  def merge_request_count
    @merge_request_count ||=
      api.count("#{merge_request_query_prefix}labels=#{team.label}&state=merged&milestone=#{release}")
  end

  private

  def api
    @api ||= GitlabApi.new(token: token)
  end

  def template
    @template ||= load_template(team.template)
  end

  def load_template(filename)
    ERB.new(File.read("templates/#{filename}.erb"), nil, '<>')
  end
end
