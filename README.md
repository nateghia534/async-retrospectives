# Async retrospective issue creator

Creates issues for use in [team retrospectives] automatically.

For more information, read the [blog post]!

[blog post]: https://about.gitlab.com/2019/03/07/how-we-used-gitlab-to-automate-our-monthly-retrospectives/

## How it works

This project has [scheduled pipelines] that create issues for specific
teams, using the [GitLab-Bot] user. These run as follows:

* On the **27th** (the day after the previous retrospective closes), it
  creates a 'skeleton issue'. This contains the general description of
  what the retrospective is for.
* On the **16th**, it optionally updates the existing issue's description
  with:
    * All ~Deliverable issues the team shipped that month.
    * Any ~missed-deliverable issues.
    * Any ~follow-up issues from previous retrospectives that are still
      open.

  It also mentions the team to encourage them to contribute, and
  optionally creates some discussions on the issue for the team to use
  during the retrospective.

[team retrospectives]: https://about.gitlab.com/handbook/engineering/management/team-retrospectives/
[scheduled pipelines]: https://gitlab.com/gitlab-org/async-retrospectives/pipeline_schedules
[GitLab-Bot]: https://gitlab.com/gitlab-bot

## Adding a new team

To add a new team, we need to:

1. Create a project for that team in [gl-retrospectives]. If you don't have
   permissions to create it, [open an issue](https://gitlab.com/gitlab-org/async-retrospectives/issues/new?issuable_template=Request_project&issue[title]=New%20project%20request).
2. Update [`teams.yml`](teams.yml) with the team info. See the comments
   in that file for information on the various options.

Then, the pipeline schedules above will take care of the rest!

[gl-retrospectives]: https://gitlab.com/gl-retrospectives

### Templates

We use [ERB] templates for creating issues. These templates have access
to the variables you can see in the [default template], as well as a
proc, `include_template`, that will render and insert another template
in its place. So this:

```erb
This is the main template.

<%= include_template.call('other_template') %>
```

Will contain the result of rendering `other_template`.

[ERB]: https://ruby-doc.org/stdlib/libdoc/erb/rdoc/ERB.html
[default template]: templates/default.erb

## Running locally

```shell
$ bundle
$ bundle exec ruby retrospective.rb # Shows options
```

For example, this will write the issue description for a Plan retrospective to
standard output. (To actually create an issue, remove `--dry-run`.)

```shell
$ bundle exec ruby retrospective.rb create --dry-run --token="$GITLAB_API_TOKEN" --team=Plan
```

## The gl-retrospectives group

The [gl-retrospectives group][gl-retrospectives] is a new top-level group, not
inside gitlab-com or gitlab-org.

This is because retrospectives may be confidential to the stage group or team
while they're being discussed. If the project was in a group like gitlab-com,
then everyone with access to gitlab-com could see those confidential issues.

To work around this, we have a separate top-level group with limited access.

## Contributing

Contributions are welcome! See [CONTRIBUTING.md] and the [LICENSE].

[CONTRIBUTING.md]: CONTRIBUTING.md
[LICENSE]: LICENSE
