require 'httparty'

class GitlabApi
  ENDPOINT = 'https://gitlab.com/api/v4'

  UpdateFailed = Class.new(StandardError)

  def initialize(token: nil)
    @token = token
  end

  def count(path)
    # When fetching the count it's safe to include confidential items too

    get(path, auth: true).headers['X-Total']
  end

  def get(path, auth: false)
    path = "#{ENDPOINT}/#{path}" unless path.start_with?(ENDPOINT)

    options = {}
    options[:headers] = { 'Private-Token': token } if auth

    HTTParty.get("#{path}&per_page=50", options).tap do |response|
      check_response_code!(:get, path, response)
    end
  end

  def post(path, body:)
    post_or_put(path, body: body, method: :post)
  end

  def put(path, body:)
    post_or_put(path, body: body, method: :put)
  end

  private

  attr_reader :token

  def post_or_put(path, body:, method:)
    path = "#{ENDPOINT}/#{path}" unless path.start_with?(ENDPOINT)

    options = {
      body: body,
      headers: { 'Private-Token': token }
    }

    HTTParty.send(method, path, options).tap do |response|
      expected_code = method == :post ? 201 : 200

      check_response_code!(method, path, response, expected_code: expected_code)
    end
  end

  def check_response_code!(method, path, response, expected_code: 200)
    return if response.code == expected_code

    puts "#{method.to_s.upcase} to #{path} failed!"
    puts ''
    puts "Response code: #{response.code}. Body:"
    puts response.body

    raise UpdateFailed
  end
end
